Operation Solutions's Drone Detection & Tracking Server
--------------------------------------------------------
  
    
.           
Clone the software using your username and password authorised for the repository.  
    $ git clone https://jtk19@bitbucket.org/OperationalSolutions/dronedetect.git  
  
Unzip the models in the build directory  
    $ cd dronedetect/drone_detect/build  
    $ unzip goturn.zip  
      

The following software packages and their dependencies need to be installed   
on the Linux server prior to compilation.  It is recommended that the server be  
one with a powerful NVIDIA GPU for CUDA based GPU acceleration; though at this   
point in time GPU acceleration is not yet implemented, this is expected to be  
done in the near future.  

NVIDIA drivers and CUDA  
OpenCV 4.1.0 and above compied with CUDA acceleration options    
Python 3.6 and above  
The latest Boost library including the compiled libraries    
The delendencies for the above and all C/C++ build tools    
  
These packages are generally installed into /usr/local paths. The Makefiles in     
the source directories need to be changed to the paths they are installed into.     
     
Build the software tree making any relevant changes into to the Makefiles to  
point to the installation paths of the dependencies.  
    $ cd ..  
    $ make all  
  
Make a sub-directory in /home/data/video and put all your videos in the sub directory.  
    $ sudo mkdir -p /home/data/video  
    $ sudo chown -R < username >:< groupname > /home/data/video  
    $ mkdir /home/data/video/test1  
    $ cp -r < video > /home/dta/video/test1  
    
Enter the sub-directories with videos in the config file in the build directory.  
Also ensure that the extensions of your videos are all included in the extensions variable.  
    $ cd build  
    $ cat drone.cfg  
veed_video_subdir=test1  
veed_video_extensions=mkv,avi,mp4,MOV,mov  
  
Run the executable built in the build directory by the compilation.    
    $ ./track

  