#ifndef __MOBILEOBJECT_H__
#define __MOBILEOBJECT_H__


#include <vector>
#include <deque>
#include <math.h>
#include <algorithm>
#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>

#include <vision_def.h>


#define DR_STATS_HISTORY_LEN	( DR_FRAME_RATE / 2)


using namespace std;

struct TrackedObject
{
	TrackedObject( cv::Rect2d loc )
	: location(loc), id(id_index)
	{ }

	cv::Rect2d location;
	size_t id;

	static size_t id_index;
};


struct MobileObject
{
public:
	MobileObject()
	: _top(0), _btm(0), _lft(0), _rht(0),
	  _centre( cv::Point(0,0)), _radius(0)
	{}

	MobileObject( size_t top, size_t btm, size_t lft, size_t rht)
	: _top(top), _btm(btm), _lft(lft), _rht(rht),
	  _centre( cv::Point( (lft + rht)/2, (top + btm)/2 ) ), _radius(0)
	{
		_radius = std::max<size_t>( (top-btm)/2, (rht-lft)/2 );
	}

	MobileObject( cv::Point c, size_t r )
	: _top(c.y - r), _btm(c.y + r),
	  _lft(c.x - r), _rht(c.x + r),
	  _centre(c), _radius(r)
	{}

	MobileObject( cv::Rect r )
	: _top(r.y), _btm(r.y + r.height),
	  _lft(r.x), _rht(r.x + r.width),
	  _centre( cv::Point( r.x + 0.5*r.width, r.y + 0.5*r.height)),
	  _radius( std::max( r.width, r.height) )
	{}

	void setCentre()
	{
		_centre = cv::Point( 0.5 * (_lft + _rht), 0.5 * (_top + _btm) );
	}
	void setEdges()
	{
		_lft = _centre.x - _radius;
		_rht = _centre.x + _radius;
		_top = _centre.y - _radius;
		_btm = _centre.y + _radius;
	}

	bool contains( cv::Point pt )
	{
		return ( ( ((size_t)pt.x) > _lft ) && ( ((size_t)pt.x) < _rht )
				&& ( ((size_t)pt.y) > _top ) && ( ((size_t)pt.y) < _btm ) );
	}

	void clear() { objectRef.clear(); }

	cv::Rect getRect() const
	{
		return cv::Rect( _lft, _top, (_rht - _lft), (_btm - _top) );
	}

	long area() const { return getRect().area(); }
	size_t width() const { return ( _rht - _lft ); }
	size_t height() const { return ( _btm - _top ); }
	cv::Point centre() const { return cv::Point( 0.5 * (_lft + _rht), 0.5 * (_top + _btm) ); }


public:
	size_t _top, _btm;
	size_t _lft, _rht;

	cv::Point _centre;
	size_t _radius;

	static vector<string> objectRef;
	static size_t objectId;

};


struct MobileObjectFrame
{
public:

	MobileObjectFrame()
	: _detected(false),
	  _detected_roi(0,0,0,0),
	  _maxMotionAxes(0,0)
	{}

	/*void addOptFlowObject( MobileObject &obj )
	{
		obj.setCentre();
		_optFlowObj.push_back( obj );
	}*/

	void addHistMaskObject( MobileObject &obj )
	{
		_histMaskObj.push_back( obj );
	}

	void addAxes( int x, int y )
	{
		_maxMotionAxes.x = x;
		_maxMotionAxes.y = y;
	}

	bool singleDroneDetect();

public:

	bool _detected;
	cv::Rect _detected_roi;
	string _detected_name;

	//vector<MobileObject> _optFlowObj;
	vector<MobileObject> _histMaskObj;
	cv::Point _maxMotionAxes;

	vector<TrackedObject> _trackedObj;
};



class MobileObjectHistory
{
public:

	MobileObjectHistory()
	  : _objectSum(0)
	{}

	void clearHistory() { _obj.clear(); }

	void addObject( const MobileObject &ob );
	void newObject( const MobileObject &ob ) { _newObj = ob; }
	void acceptNewObject( bool forceAccpet = false);

	bool newObjectGood() const;
	bool newObjectGood( const MobileObject &ob );

	void addRegionInfo( size_t mobileRegions );
	bool isRegionGood() const;

private:

	deque<MobileObject> _obj;
	MobileObject _newObj;

	deque<size_t> _objectCount;
	size_t _objectSum;
};



#endif
