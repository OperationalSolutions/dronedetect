#ifndef __DRONE_TRACKER_H__
#define __DRONE_TRACKER_H__


#include <iostream>
#include <vector>

#include <opencv2/opencv.hpp>
#include <opencv2/tracking.hpp>


using namespace std;
using namespace cv;


class OnlineTracker
{

public:

	enum Algo_T { ALGO_KCF = 0, ALGO_MIL, ALGO_CNN, DR_NUM_ALGO };

	friend void runTracker( OnlineTracker::Algo_T algo, OnlineTracker *tr_obj );

public:

	OnlineTracker() = default;

	void init( Mat &frame, cv::Rect loc, bool initAll = true );
	bool track( Mat &frame);

	bool getLocation( Algo_T algo, Rect &loc )
	{
		if ( _ok[algo] )
		{
			loc = _loc[algo];
		}
		return _ok[algo];
	}

	Rect2d consensus();

	int rows() { return _frame.rows; }
	int cols() { return _frame.cols; }


private:

	static const char *algo_str[];
	static const cv::Scalar col[];

	static cv::Point centre( cv::Rect2d r )
	{
		return cv::Point( r.x + 0.5 * r.width, r.y + 0.5 * r.height );
	}

private:

	bool step_frame = false;

	cv::Mat _frame;

	vector<Rect2d> _loc;
	vector<bool> _ok;

	cv::Rect2d _consensus_loc;

	static Ptr<Tracker> miltr;
	static Ptr<Tracker> kcftr;
	static Ptr<Tracker> cnntr;

	bool reInit( Algo_T algo, cv::Rect2d r );

};



#endif
