
#include <boost/tuple/tuple.hpp>
#include <math.h>
#include "gplot.h"



void GPlot::plot_def( string xrange, string yrange )
{
	string plt = string("set xrange [") + xrange + "]\n"
				+ "set yrange [" + yrange + "]\n";
	_gp << plt.c_str();
}


void GPlot::plot_bar( string title, vector<double> y, string xrange, string yrange )
{
	vector< boost::tuple<int, double> > vec;

	for ( size_t i = 0; i < y.size(); ++i )
	{
		vec.push_back( boost::make_tuple( i, y[i] ) );
	}
	if ( xrange.empty() )
	{
		_gp << "set autoscale x\n";
	}
	else
	{
		_gp << "set xrange ["<< xrange<< "]\n";
	}
	if ( yrange.empty() )
	{
		_gp << "set autoscale y\n";
	}
	else
	{
		_gp<< "set yrange ["<< yrange<< "]\n";
	}
	_gp << "plot '-' with lines lt rgb \"blue\" title '"<< title<< "'\n";
	_gp.send1d( vec );
	_gp.flush();
}

void GPlot::plot_bar( string title, cv::Mat m, int row )
{
	vector< boost::tuple<int, double> > vec;

	for ( int i = 0; i < m.cols; ++i )
	{
		double d = m.at<double>( row, i );
		vec.push_back( boost::make_tuple( i, d ) );
	}
	//_gp << "set xrange [-1:1280]\nset yrange [0:28]\n";
	_gp << "set xrange [-1:1280]\nset autoscale y\n";
	_gp << "plot '-' with lines lt rgb \"blue\" title '"<< title<< "'\n";
	_gp.send1d( vec );
	_gp.flush();
}
