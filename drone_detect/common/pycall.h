#ifndef __PHCALL_H__
#define __PYCALL_H__

#include <iostream>
#include <Python.h>
#include <vector>
#include <map>
#include <thread>


#define PYTHON_PROG_DIR	"../python"
#define PYTHON_TMP_DIR	"../tmp"
#define NULL_STRING	""
#define PY_BAR_MODULE	"plots"
#define PY_BAR_PLOTS	"optFlowGraphs"


using namespace std;


class PyCall
{
public:

	static void plotOptFlowGraphs( vector<double> x, vector<double> y );

private:


};

#endif
