#ifndef	__GNU_PLOT_H__
#define __GNU_PLOT_H__

#include <vector>
#include <iostream>
#include <opencv2/core/mat.hpp>

#include <gnuplot-iostream.h>


using namespace std;


class GPlot
{
public:

	void plot_def( string xrange = "-1:1280", string yrange = "-0:28" );
	void plot_bar( string title, vector<double> y,
				   string xrange = "-1:1280", string yrange = "-0:28" );
	void plot_bar( string title, cv::Mat m, int row );

private:

	Gnuplot _gp;
};

#endif
