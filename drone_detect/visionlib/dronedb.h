#ifndef __DRONEDB_H__
#define __DRONEDB_H__

#include <iostream>
#include <opencv2/opencv.hpp>


using namespace std;
using namespace cv;


namespace Drone
{

class DroneDb
{
public:

	static string saveWebClip( Mat clip );


};


} // end namespace Drone


#endif
