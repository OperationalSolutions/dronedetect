
#include <vector>
#include <sstream>

#include <util.h>
#include "vision_def.h"
#include "dronedb.h"



namespace Drone
{


string DroneDb::saveWebClip( Mat clip )
{
	stringstream ss, ss_url;
	vector<string> subdir, files;
	int subdir_count, file_count, rtn;
	bool make_new_dir = false;
	string saved;

	string path(DR_DRONEDB_PATH);
	path += string("/") + DR_DRONEDB_FACE_SUBDIR;
	ss_url<< DR_DRONEDB_URL_STUB;

	subdir_count = common::listdir( subdir, path );
	if ( subdir_count < 0 )
	{
		cerr<< "[DroneDb::saveWebClip] Failed to read path: "<< path<< endl;
		return saved;
	}
	else if ( subdir_count > 0 )
	{
		ss << path << "/dr"<< (subdir_count-1);
		file_count = common::listdir( files, ss.str() );
		if ( file_count < 0 )
		{
			cerr<< "[DroneDb::saveWebClip] Failed to read clip directory: "<< ss.str()<< endl;
			return saved;
		}
		else if ( file_count > DR_DRONEDB_MAX_CLIPS_IN_DIR )
		{
			ss << path << "/dr"<< subdir_count;
			ss_url << "/dr"<< subdir_count;
			make_new_dir = true;
		}
		else // file count in [0 ... MAX], save to current subdir
		{
			ss_url << "/dr"<< (subdir_count - 1);
			make_new_dir = false;
		}
	}
	else	// subdir_count == 0
	{
		cerr<< "path1 url: "<< ss_url.str()<< endl;
		ss << path << "/dr0";
		ss_url<< "/dr0";
		file_count = 0;
		make_new_dir = true;
	}

	if ( make_new_dir )
	{
		rtn = common::create_dir( ss.str() );
		if ( rtn < 0 )
		{
			cerr<< "[DroneDb::saveWebClip] Failed to create new clip directory: "<< ss.str()<< endl;
			return saved;
		}
	}

	ss << "/di"<< file_count<< ".jpg";
	ss_url << "/di"<< file_count<< ".jpg";
	if ( !cv::imwrite( ss.str(), clip) )
	{
		cerr<< "[DroneDb::saveWebClip] Failed to save clip to: "<< ss.str()<< endl;
		return saved;
	}
	saved = ss_url.str();
	cerr<< "\nSaved to: "<< saved<< endl;

	return saved;
}









}
