#ifndef	__DRONE_VISION_H__
#define __DRONE_VISION_H__

#include <iostream>
#include <opencv2/opencv.hpp>

#include <mobileObject.h>


#define	DR_CAM_RTSP_PORT		554


using namespace std;
using namespace cv;


namespace Drone
{


class Motion_Detect
{
public:

	static size_t detectMaskObjects( Mat mask,  MobileObjectFrame &fr,
										Mat frame = Mat(), bool setBorder=true );
	static size_t detectMobileObjects( Mat fgray, Mat fgray_prev, MobileObjectFrame &fr_obj,
									 	Mat frame = Mat(), bool setBorder=true );
	static cv::Rect getNearestMobileObject( size_t x, size_t y, string camStreamUrl );

	static cv::Mat getCamClip( string camIp, string camUsr = "admin", string camPwd = "sts1234@" );
	static cv::Mat getCamClip( string camIp, string camUsr, string camPwd, int x, int y, int w, int h );

};



}
#endif
