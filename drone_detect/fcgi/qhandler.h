#ifndef __DR_QHANDLER_H__
#define __DR_QHANDLER_H__

#include <iostream>
#include <deque>
#include <vector>
#include <boost/algorithm/string.hpp>

#include <vision_def.h>


#define	DR_QR_CLIP		"clip"


using namespace std;


class QueryHandler
{

public:

	static string handle_query( string query );

	QueryHandler( string query )
	: _parsed(false), _q(query), _respondHtml(true)
	{
		if ( !_q.empty() )
		{
			boost::algorithm::to_lower(_q);
		}
	}

	void parse();

	string query() const { return _q; }
	string call() { if (!_parsed) parse(); return _call; }
	vector<string>& arg() { if (!_parsed) parse(); return _arg; }
	size_t numargs() { if (!_parsed) parse(); return _arg.size(); }

	bool respondHtml() { if (!_parsed) parse(); return _respondHtml; }

private:

	bool _parsed;
	std::string _q;
	std::string _call;
	bool _respondHtml;
	std::vector<string> _arg;

	static size_t min_args[];
};





#endif
