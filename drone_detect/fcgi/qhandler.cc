
#include <stdexcept>
#include <algorithm>
#include <exception>
#include <sstream>
#include <stdlib.h>
#include <opencv2/opencv.hpp>

#include <drone_vision.h>
#include <dronedb.h>
#include "qhandler.h"

using namespace cv;


size_t QueryHandler::min_args[] = { 2 };


string QueryHandler::handle_query( string query )
{
	QueryHandler qr(query);
	stringstream msg;

	std::stringstream cerr_str;
	std::cerr.rdbuf( cerr_str.rdbuf() );

	try
	{
		qr.parse();
	}
	catch (exception &ex )
	{
		msg << "-1"<< endl;
		msg<< ex.what()<< endl;
		return msg.str();
	}

	if ( qr.call() == string(DR_QR_CLIP) )
	{
		cv::Mat img = Drone::Motion_Detect::getCamClip( DR_CAM1_IP );
		string url = Drone::DroneDb::saveWebClip( img );
		string purl = string(DR_HTTP_HOST) + url;
		//boost::replace_all( purl, ".jpge", ".jpg" );
		cerr<< "\n image url: ["<< purl<< "]"<< endl;
		if ( qr.respondHtml() )
		{
			msg << "Content-type: text/html\r\n\r\n";
			msg << "<title>OSL Autonomous DroneDetect</title>";
			msg << "Query : ";
			msg << query;

			string err = cerr_str.str();
			boost::replace_all( err, "\n", "<p>" );
			msg << "<p>" << err << "<p><p>";

			msg << "<p><img style=\"height:300px\"";
			msg << string("src=\"");
			msg << ((purl.empty() ) ? "/dronedb/dr1/motion_det_track_6.jpg" : purl);
			msg << "\"> <p>";
		}
		else
		{
			msg << "Content-type: text/plain\r\n\r\n";
			msg << string(DR_HTTP_HOST);
			msg << ( (url.empty() ) ? "/dronedb/dr1/motion_det_track_6.jpg" : url );
		}
	}
	else
	{
		msg << "-1"<< endl;
		msg<< "[QueryHandler::handle_query] Undefined call ["<< qr.call()<< "] in query."<< endl;
	}

	return msg.str();
}



void QueryHandler::parse()
{
	if ( _q.empty() )
	{
		throw runtime_error( "[QueryHandler::parse] query empty.");
	}

	deque<string> a;
	_arg.clear();
	boost::split( a, _q, boost::is_any_of(",;:") );

	if ( a.size() > 0 )
	{
		_call = a[0];
		a.pop_front();
	}
	else
	{
		throw runtime_error( "[QueryHandler::parse] query provided no call or arguments." );
	}

	_respondHtml = false;
	if ( a.size() > min_args[0] )
	{
		if ( atoi( a[a.size() - 1].c_str() ) > 0 )
		{
			_respondHtml = true;
		}
		a.pop_back();
	}

	std::copy( a.begin(), a.end(), std::back_inserter(_arg));

	_parsed = true;
}


