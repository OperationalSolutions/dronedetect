#ifndef __DRONE_ORIENT90_CONFIG_H__
#define __DRONE_ORIENT90_CONFIG_H__

#include <iostream>
#include <vector>



#define DISPLAY_VIDEO_OPTICALFLOW

//#define DISPLAY_VIDEO_ORIENT90		// Un-comment this to display video within orient90.
#define ORIENT90_SINGLE_FILE_RUN		// Run orient90 for a single video file. Comment out for testing only.



#define DRONE_ORIENT90_CONFIG_FILE			"drone.cfg"

#define DRONE_NUM_PARALLEL_PROCS			16

#define DRONE_HOME							getenv("HOME")
//#define DRONE_BUILD_EXEC_PATH_STR			(std::string(DRONE_HOME) + "/src/veed/veed/src/build/")

#define DRONE_VIDEO_DATA_DIR				"/home/data/video"
#define DRONE_VIDEO_DATA_DIR_STUB			"home/data/video"
#define DRONE_DATA_DIR						"/home/data"
#define DRONE_DETECT_DIR					"/home/data/detect"
#define DRONE_WRITE_DIR						"/home/data/write"

#define DRONE_FRAME_ROWS_DEFAULT	720
#define DRONE_FRAME_COLS_DEFAULT	1280


using namespace std;


enum Drone_Orient_T
{
	DRONE_ORIENT_0 = 0,			// original frame with no turning
	DRONE_ORIENT_90 = 1,			// needs 90 degree clockwise rotation, 1
	DRONE_ORIENT_180 = 2,		// needs 180 degree clockwise rotation, 2
	DRONE_ORIENT_270	= 3			// needs 270 degree clockwise rotation, 3
};

extern string orient_str[4];

class Drone_Config
{
  public:
	Drone_Config( string cfgFile = DRONE_ORIENT90_CONFIG_FILE );
	~Drone_Config() = default;

	int getVideoInSubdir( vector<string> &video_vec, string subdir );

  public:

	//string	video_relative_dir;
	vector<string>	video_subdir;
	vector<string>	video_ext;
};

#endif
